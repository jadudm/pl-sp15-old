---
title: At a Glance
hideTitle: true
layout: default
---

<div class="text-center">
  The video below introduces you to Cel.ly.<br/>
    Please sign up for an account...<br/>
<iframe width="640" height="360" src="//www.youtube.com/embed/CHAU4qjD-i4?rel=0" frameborder="0" allowfullscreen></iframe>
<br/>
... and then join <b><a href="http://cy.tl/1CKqzez">@ProgrammingLanguages</a></b>.<br/>
<small>(Your profile must include an email address ending in <b>berea.edu</b> to join.)</small><br/>
</div>

{% for todo in site.data.todo %}

{% unless todo[1].hide %}

<div class="row">
  <div class="col-md-10 offset-1">
    <h3>{{todo[1].question}}</h3>
  </div>
</div>
  {% for part in todo[1].parts %}
  {% assign theID = part.title | remove: " " | remove: "." | remove: "," | remove: "-" | remove: "+" | remove: "&" %}
  {% capture datelength %}{{ part.date | size | minus: 9 }}{% endcapture %}
  {% capture date %}{{part.date}}{% endcapture %}
  {% if datelength contains '-' %}
    {% if site.classtime %}
      {% capture date %}{{part.date}}{{site.classtime}}{% endcapture %}
    {% else %}
      {% capture date %}{{part.date}}2359{% endcapture %}
    {% endif %}
  {% endif %}

  <div class="row"> 
    <div class="col-md-2" id="{{theID}}-{{date}}"> </div>
    <div class="col-md-8" style="padding-bottom: 10px;">
      <a href="todo/{{todo[0]}}/{{part.file}}.html">
        {{part.title}}
      </a>
      <span style="font-size: 80%; padding: 10px;" class="pull-right" id="{{theID}}-prettydate"> </span>
    </div>
  </div>
  <script type='text/javascript'>
    function {{theID}}{{date}} () {
      //console.log("Running {{theID}}{{date}}\n");
      dueIn("{{theID}}-{{date}}", "{{date}}", "{{part.inclass}}", "", "90%");
      prettyDate("{{theID}}-prettydate", "{{date}}", "");
    }
    {{theID}}{{date}}();
    setInterval('{{theID}}{{date}}()', 1000 * 60);
  </script>
{% endfor %}

{% endunless %}
{% endfor %}
