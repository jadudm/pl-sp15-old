local:
	jekyll serve --host ${IP} --port ${PORT} -w --config _config.yml,_config_local.yml

remote:
	jekyll build --config _config.yml,_config_remote.yml
	python ./_cleanup_ics.py
	rsync -p --chmod=Fa+r -vrz \
		-e ssh _site/ \
		jadudm@jadud.com:~/jadud.com/teaching/pl-sp15
test:
	jekyll build --config _config.yml,_config_remote.yml
	python ./_cleanup_ics.py
