---
title: Getting Alerts with Updates
resource: local
---

When I update the website, there is a file that gets updated with a short note about what has changed. There are multiple ways to stay alerted to changes in this site. The most requested in recent semesters was to receive a text (or SMS) whenever I update the site. This is now possible.

* [Get a Text Message](#get-a-text-message)
* [Subscribe to a Calendar](#subscribe-to-a-calendar)

If there is demand, I will write up instructions for the following. Each works differently, but might appeal to different students in different ways.

* Follow a Twitter Feed
* Get an Email
* Using a Feed Reader

All of these rely on IFTTT, but I have not (yet) set up calendars and so on.

## Get a Text Message

You can use the tool [If This Then That (IFTTT)]({{site.data.urls.ifttt}}) to get messages whenever the website is updated. To do this, you'll need to create a free account at [IFTTT]({{site.data.urls.ifttt}}). Once you do that, you'll need to create a **recipe**. These are easy to make; once you learn how to make one for this class, there are many others you can explore and create for yourself for many other purposes.

### Step 1: Create a Recipe

Click on the "Create Recipe" button to create a new recipe.

{% include rimg src="ifttt-001.png" %}

### Step 2: Click "this"

{% include rimg src="ifttt-002.png" %}

### Step 3: Find the "Feed" Action

I search for the word "feed." You can also look around and find the orange broadcast icon.

{% include rimg src="ifttt-003.png" %}

### Step 4: Trigger on "New Feed Item"

{% include rimg src="ifttt-004.png" %}

### Step 5: Set the Feed URL

This should be **{{site.url}}/{{site.alertsfile}}**

{% include rimg src="ifttt-005.png" %}

### Step 6: Click "that"

{% include rimg src="ifttt-006.png" %}

### Step 7: Choose the SMS Action

You may have to set up the SMS action first. It sends you a text that gives you a secret code; you enter it, and then you're allowed to send texts to your phone. (This prevents you from SMS-bombing a friend of yours, basically.)

{% include rimg src="ifttt-007.png" %}

### Step 8: Choose the "Send me an SMS"  Action

{% include rimg src="ifttt-008.png" %}

### DONE!

That should be it. Now, whenever the course website updates, you'll get an alert on your phone. 


## Subscribe to a Calendar

You should be able to subscribe to the following calendar feed:

**{{site.url}}/todo.ics**

in Google Calendar or Outlook. This will add the homework deadlines for this class to your calendar.

