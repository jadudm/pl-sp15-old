---
title: Course Goals
resource: local
layout: default
---

The course learning goals provide the "big picture" for what we will study this term. They don't spell out individual assignments or particular learning objectives, but instead define the broad areas of study that we will engage in over the course of the term.

## Outline

1. [Functional Programming](#fp)
1. [Language Implementation through Interpretation](#interp)
1. [Strategies for Learning / Breadth of Languages](#breadth)
1. [Collaboration and Cooperation](#coop)

## Learning Goals

1. <a name="fp"></a> **Functional Programming**. <br/> 

    Designing programs in the functional style puts an emphasis on understanding the structure of our data and expressing the flow of control in our programs as a sequence of function calls that do not mutate, or change, the state of our data. This style of programming has its roots in the *lambda calculus*, which is used for  reasoning about programs and proving properties of the languages we work with.

1. <a name="interp"></a> **Language Implementation through Interpretation**. <br/>

    Programs written in a language are expressed in a *concrete syntax* which is then parsed into an *abstract syntax tree* and then either transformed (compiled) or evaluated. The latter happens every time you write code in Python. In this class, you will study how we implement a programming language by writing the code that does the parsing and evaluation. We call programs like this *interpreters*, because they *interpret* (rather than compile) programs.

1. <a name="breadth"></a> **Strategies for Learning / Breadth of Languages**. <br/>

    We will explore functional programming and write interpreters in the functional style using the programming language [Racket](http://racket-lang.org/), which is a variant of the programming language [Scheme](http://www.r6rs.org/). However, we also want to experience a wide range of languages: those that are fundamentally concurrent, declarative, object-oriented, or otherwise. Therefore, while we are developing a deep knowledge of functional programming (by writing interpreters), we will also be developing a breadth of experience that will ultimately serve as a foundation for the learning of every new language we encounter for the rest of our lives.

1. <a name="coop"></a> **Collaboration and Cooperation**. <br/>
    
    Software is built by teams, and those teams are not always co-located in the same space. Learning to appreciate and study and relate to diverse peoples and cultures, thinking about how to work with people you don't always agree with, and how to manage the complex relationships that are part of the working world are all essential parts of being a software developer today. While not "technical" learning like that described above, it is an essential part of a liberal arts education in computing and languages today.

