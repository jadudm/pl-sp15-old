---
title: Software
resource: local
layout: default
---

## TinyASM (Windows)

This is the Windows GUI version of TinyASM.

* [20141030](https://drive.google.com/file/d/0B_8znW3VOlkGS3hFbElOVXNsT2c/view?usp=sharing) 
  Fixed the placement of R0 through R15.

* [20141029](https://drive.google.com/file/d/0B_8znW3VOlkGV0FnNE5HZzZ6Zmc/view?usp=sharing) 
  Fixed label location placement, as well as a number of other features.
  
## TinyASM (Linux)

This is the Linux command-line binary of TinyASM. I recommend following the instructions below.

First, create a directory where you can put TinyASM.

    mkdir ~/bin

This puts the folder "bin" in your home directory. Next, change into that directory.

    cd ~/bin

Now, you're ready to cook with steam heat. (That's a popular phrase from back in the day.)

To download TinyASM, paste this into your terminal in the directory where you want to run TinyASM:

    curl -o tiny.tar.gz http://jadud.com/teaching/comporg-f14/resources/tiny.tar.gz

Then, you need to expand the compressed tarball:

    tar xvzf tiny.tar.gz

This will leave the directory **tiny** in your folder. 

Now, you should add the following line to your **.profile**. First, go into your home directory:

    cd

Then, list all of the files, including the invisible ones:

    ls -al

Now, left-click on the file called **.profile**, so you can open it in an editor. Near the top of this file, add the following line:

    export PATH=~/bin/tiny/bin:$PATH

Add that line exactly as written. If you do it wrong, you'll know, because things will stop working. The squiggle is a tilde, and on most keyboards it is a shifted character to the left of the one. (I would say it was "SHIFT-BACKTICK," but that assumes you refer to the key to the left of the 1 as a backtick.)

Now, restart your terminal. You can do this by closing it and opening another. (You can also right-click on the terminal itself and select *Restart All Terminal Sessions*.)

To run tinyasm, you should be able to type

    tinyasm -h

anywhere, and it will respond with instructions. If you get a "command-not-found" type error, feel free to contact me.

to generate a **rom.h** file:

    tinyasm --rom yourcode.asm

Note that the input file must end in **.asm**, or TinyASM will reject it. Also, the rom.h file will be overewritten every time you re-run TinyASM.
 
## Revision History
* [20141204](http://jadud.com/teaching/comporg-f14/resources/tiny.tar.gz)
  Fixed the build so that it will work when people install it. 
 
* [20141202](http://jadud.com/teaching/comporg-f14/resources/tinyasm-20141202)
  The first release.