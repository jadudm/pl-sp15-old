---
title: Programming Languages Syllabus
resource: local
layout: default
---

{% include twocol label="Course" value=site.name %}
{% include twocol label="Number" value=site.number %}
{% include twocol label="Instructor" value="Matt Jadud" %}
{% include twocol label="Office" value="Danforth Tech 102B" %}
{% include twocol label="Phone" value=site.phone %}
{% include twocol label="Email" value="jadudm" %}


{% callout info %}
This syllabus may be amended, edited, or otherwise updated if deemed necessary by the instructor.
{% endcallout %}

## Course Learning Goals
1. [Functional Programming](#fp)
1. [Language Implementation through Interpretation](#interp)
1. [Strategies for Learning / Breadth of Languages](#breadth)
1. [Collaboration and Cooperation](#coop)

## Learning Goals

1. <a name="fp"></a> **Functional Programming**. <br/> 

    Designing programs in the functional style puts an emphasis on understanding the structure of our data and expressing the flow of control in our programs as a sequence of function calls that do not mutate, or change, the state of our data. This style of programming has its roots in the *lambda calculus*, which is used for  reasoning about programs and proving properties of the languages we work with.

1. <a name="interp"></a> **Language Implementation through Interpretation**. <br/>

    Programs written in a language are expressed in a *concrete syntax* which is then parsed into an *abstract syntax tree* and then either transformed (compiled) or evaluated. The latter happens every time you write code in Python. In this class, you will study how we implement a programming language by writing the code that does the parsing and evaluation. We call programs like this *interpreters*, because they *interpret* (rather than compile) programs.

1. <a name="breadth"></a> **Strategies for Learning / Breadth of Languages**. <br/>

    We will explore functional programming and write interpreters in the functional style using the programming language [Racket](http://racket-lang.org/), which is a variant of the programming language [Scheme](http://www.r6rs.org/). However, we also want to experience a wide range of languages: those that are fundamentally concurrent, declarative, object-oriented, or otherwise. Therefore, while we are developing a deep knowledge of functional programming (by writing interpreters), we will also be developing a breadth of experience that will ultimately serve as a foundation for the learning of every new language we encounter for the rest of our lives.

1. <a name="coop"></a> **Collaboration and Cooperation**. <br/>
    
    Software is built by teams, and those teams are not always co-located in the same space. Learning to appreciate and study and relate to diverse peoples and cultures, thinking about how to work with people you don't always agree with, and how to manage the complex relationships that are part of the working world are all essential parts of being a software developer today. While not "technical" learning like that described above, it is an essential part of a liberal arts education in computing and languages today.



## Course Learning Objectives

Learning objectives are the assessable points of learning in the course. The number of these learning objectives you complete is what determines your final grade in this course.

These objectives are subject to change.

## Outline

1. [(Mostly) Functional Programming in Racket](#mostly-functional-programming-in-racket)

1. [Representation and Interpretation of Languages](#representation-and-interpretation-of-languages)

1. [Learning New Languages](#learning-new-languages)

## (Mostly) Functional Programming in Racket

It is expected that it will take approximately 7 weeks to cover this material, assuming that 60% of your time is being spent on learning related to functional programming in Racket.

Objectives marked with a **?** are particularly subject to change/omission based on the lived experience of the course.

### Level 1

1. Write state-free programs over atomic data.

1. Develop tests that cover representative inputs and branches in code.

1. Demonstrate knowledge of tools for writing code (IDE, documentation)

1. Survey breadth of the standard library for software development in Racket.

### Level 2

1. Write state-free programs over structured data.

1. Discuss scope as it pertains to values bound within and passed between functions.

1. Compare/contrast functional and object-oriented programming as it pertains to notions of scope (eg. Racket/Python/C++).

1. Write useful functions using *map*, *fold*, and *filter*.

### Level 3

1. Write state-free programs over built-in lists.

1. Write state-free programs over self-defined, self-referential data.

1. Reason about scope in the presence of closures and compare/contrast with anonymous classes. **?**

1. Discuss the role of functional languages in the context of big data, concurrency, and other styles of programming. (For example, big data, safety, web-based systems, and other current challenges in computing.) **?**

1. Write useful functions that return functions.

## Representation and Interpretation of Languages

It is assumed that the implementation language for these objectives is Racket, and that it will take approximately 7 weeks to complete these objectives at 60% effort.

Objectives marked with a **?** are particularly subject to change/omission based on the lived experience of the course.


### Level 1

1. Describe an AST for a small language (eg. a BNF or EBNF).

1. Define appropriate structures for parsed output representation.

1. Parse programs in a simple, 1-type language.

1. Compare/contrast with interpreter with bytecode interpretation. **?**

1. Implement basic tests for a small interpreter.

### Level 2

1. Extend a 1-type language with conditional structures and a second type.

1. Discuss/describe types as sets.

1. Implement ribcage environments for implementing scope. 

1. Compare and contrast different approaches to scoping.

1. Implement tests for new language features.

### Level 3

1. Add rudimentary type checking to the interpreter.

1. Extend the interpreter to include 1st-class functions.

1. Compare/contrast static vs. dynamic type systems in languages.

1. Discuss the nature/undecidability of type systems.

## Learning New Languages

It is assumed that this learning objective will be repeated three, possibly four times over the course of the term. That is, each language will receive 3 weeks of learning time at 40% effort. There is only one level of competency associated with this learning task.

1. Demonstrate facility with the syntax of the language.

1. Participate in the online community of learners by asking and answering questions. 

1. Write at least one program of substance that exemplifies the design intent behind the language.

1. Know the basic history of the language as well as the current state of its community.

1. Compare/contrast this language with Racket in terms of design intent, binding and iteration forms, functions, structures, and memory management. (This involves *moving past the hype*.)

### Assessment

{% callout danger %}
These criteria may need to be updated at the midterm (or later), as the Spring of 2015 is the first use of these standards in this course.
{% endcallout %}

Completion of the learning objectives accounts for 90% of your final assessment in {{site.name}}. Each objective can be completed at one of two levels:

* **Excellent**. An objective can be completed with excellence. This will be marked as an "E". 

* **Proficient**. An objective can be completed with proficiency. This will be marked as a "P." It is possible, in some cases, to revise work to move from proficiency to excellence.

Otherwise, an objective is not met. This may mean that it was attempted, but not passed with proficiency, or it may mean that it was never attempted/demonstrated. The outcome is the same, regardless.

At the end of the term, grades will be assigned as follows:

* **Excellent** <br/>
    
    To earn an **A** in {{site.name}}, you must complete **all of the objectives** for the third level of each category. In addition, at least 2/3rds of those objectives (at all levels) must be met with a proficiency of "E". (For clarity, you cannot complete all of the low-level objectives with excellence, and merely demonstrate proficiency at the third level of competency. Excellence is consistent.)

* **Good** <br/> 
    
    To earn a **B** in {{site.name}}, you must complete the all of the second level of competence in each category, and at least half of the third level of competence in each category. Completion of all objectives, where more than 1/3rd are rated as "Proficient" (as opposed to excellent) will result in a "B" as well. (Meaning, if you did not consistently demonstrate excellence in the supermajority of your work, you will earn a B, which is above-average and evidence of hard work.)
    
* **Average** <br/>

    To earn a **C** in {{site.name}}, you must complete all of the second level of competence in each category.
    
* **Poor and Failing** <br/>

    Failure to complete all of the second level of competence in each category will yield a grade of **D** or **F**, as judged by the instructor.
    
Participation accounts for 10% of your final assessment in Programming Languages. It is judged by your instructor (possibly in conjunction with your peers); your participation is a reflection of the professionalism and commitment you bring to your learning and the learning of those around you.

Each missed class period may deduct 2.5% from your final grade.

{% callout info %}
If the instructor misjudges the amount of time it takes to complete the learning objectives in the course, decisions will be made as to how to adjust the assessment. At no point will you be held accountable for large amounts of work at the last minute, nor will students suffer as a result of misjudgment or poor planning on the part of the instructor.
{% endcallout %}

## Deadlines

If you cannot complete work before a deadline, you must communicate with me in advance regarding this fact.

If you are working to complete an objective, you have one week from the time feedback is given to complete revisions. If you cannot do that, and do not communicate with me, then the work may become ineligible for demonstrating competence on that objective (depending on the nature of the assignment).

**A critical part of a standards-based approach is professionalism on your part as a learner**. This means you are engaging in the work in a timely manner, spending time on the learning task (in and out of class), and are asking questions of myself and your peers early and often, as opposed to at the last minute (or worse, after deadlines have passed).

## Class Atmosphere

I want many things for students in my classes, and I very much want you to help me achieve these goals.

* I want our laboratory to be a relaxed environment where you are **comfortable trying new things** and (sometimes) failing. By "failure" I do not mean "receiving an F," but I do mean that you try things, make mistakes, and learn from them. The last bit---learning from our mistakes---is the critical part. Neither I, nor you, nor your classmates should put down or belittle a classmate for trying.

* I want you to **look forward to {{site.short}} because it is fun**. We should be comfortable with each other---humor and laughter makes the day go faster and better. That said...

* We should **work hard, and be proud of that effort**. For me, a "fun" day is one where I've worked hard and improved myself. I have done my best to design a course that will be fun because it challenges us to work hard and do new and interesting things.

* **Respect matters**. Respect for each-other, regardless of where we are from and where we are on our life journey is of utmost importance. I have a great deal of respect for your effort as a student; I show that respect by challenging you to extend your limits, and supporting you to the best of my ability as you take risks and engage with the course throughout the semester.


{% callout info %}
These policies may be amended, edited, or otherwise updated if deemed necessary by the instructor.
{% endcallout %}

## Office Hours

I am more than happy to chat, and sometimes you can stop by during office hours and I'll be free; however, I schedule my time pretty tightly, so it is possible I may ask you to make an appointment and/or follow up via email.

## Course Prerequisites

CSC 226 and CSC 236.

## Attendance

We will be actively engaging during class time in pair programming and group discussion activities throughout the term. Attendance is part of being professional as a student; each missed class may cost you up to 2.5% off your final grade.

If you arrive particularly late, leave particularly early, or fail to participate in class, I will consider you absent.

If you miss class for reasons that are not considered legitimate, you are responsible for getting caught up. That does not mean you should write me, apologize for being absent, and then ask me what you missed. Start with a kind classmate, and see how you fare.

### Legitimate Reasons You Might Be Absent

If you legitimately cannot be present, you will find a way to communicate that to me in advance.

* If you have flu-like symptoms, email me and visit the health center. The Center for Disease Control recommends you stay home for at least 24 hours after the symptoms pass---RESTING. Without evidence from the health center of your visit, you are not excused.

* Interviews

* Family Emergencies

I will do my best to work with you to make sure that you are caught up on anything you miss if you miss class for legitimate reasons.
 
## Technology Policies

Much of the work in this course will require use of the computer, so these policies are designed to help you better understand how to be effective in a technology-rich environment.

* **Laptop and Software**: We will regularly make use of laptops during class, and you are expected to have them unless explicitly stated otherwise.

* **Communication**: The course website is your primary source for information about this course. Messages about the course will often be sent by email. These are all mechanisms you would likely use in a professional position in the real world (corporate websites, internal content management systems, and email)---you are, likewise, expected to use them in a responsible and professional manner in this course.

* **Backups**: All students are expected to back-up their work on a daily basis, which includes laboratories, assignments, and quizzes. The best way to do this is to store a copy of all work in a cloud service such as Dropbox, SkyDrive, Google Drive, or to use a DVD, flash drive, or some other media. Storing multiple copies of something on your laptop is not a backup. I will not be sympathetic to lost work in any way, shape, or form.

## Evening Lab / Support

The Computing and Digital Crafts Lab is open Sunday through Thursday from 7:00 to 9:00 PM (except on evenings of convocations). The primary teaching assistant and several other TAs will be able to answer questions about the content in the course during consultations in their  Lab hours. You are strongly encouraged to make use of the help available in the Computing and Digital Crafts Lab, as well as in the instructors' office hours. Best results are obtained trying to solve problems before asking for help, and you should be prepared to show what you have already tried. Topics in this course build throughout the course, so you should be sure to do your best to keep up with the class, so as to not fall behind. No question to which you do not know the answer is "dumb" unless it goes unanswered because it remained unasked.


## Catalogue Description

This is a course on the organization of modern computer systems, how processors grew more complex over the next three decades, and how constraints on power consumption have led us back to simpler processor designs once again. Students in this course will develop, in simulation, a 1970s-era processor using only basic logic gates. Additional work, carried out in a modern system programming language, may involve the study, design, and/or implementation of linkers, interpreters, compilers, and virtual machines, and how those technologies interface with modern embedded system processors. (This course is non-credit for students who completed the course CSC 435.)

## Statement Regarding Disability

Berea College will provide reasonable accommodations for all persons with disabilities so that learning experiences are accessible. If you experience physical or academic barriers based on disability, please see Lisa Ladanyi (Disability & Accessibility Services, 110 Lincoln Hall, 859-985-3327, lisa.ladanyi@berea.edu) to discuss options. Students must provide their instructor(s) with an accommodation letter before any accommodations can be provided. Accommodations cannot be provided retroactively. Please meet with your instructor(s) in a confidential environment to discuss arrangements for these accommodations.

{% callout danger %}
This syllabus is a living document. It may change.
{% endcallout %}
