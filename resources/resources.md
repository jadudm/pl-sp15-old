---
title: Resources
resource: local
---

## Git Resources

* [A 15 Minute Interactive Git Tutorial][gittutorial] 

  A short tutorial from GitHub regarding the use of git. Interactive. You can practice.
  
* [Git for Beginners][gitstackflow]

  A mad-crazy stackoverflow post on git.
  
* [The vogella tutorial][vogella]

  With pretty formatting.
  
* [Git from git.][gitgit]

  From the actual git pages.


[gittutorial]: https://try.github.io/levels/1/challenges/1
[gitstackflow]: http://stackoverflow.com/questions/315911/git-for-beginners-the-definitive-practical-guide
[vogella]: http://www.vogella.com/tutorials/Git/article.html
[gitgit]: http://git-scm.com/docs/gittutorial