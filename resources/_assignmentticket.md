---
title: The Assignment Ticket
resource: local
layout: default
---

When you want to turn something in to receive feedback, you should:

1. Print out the assignment ticket.

2. Hand it to me.

[The Assignment Ticket](https://drive.google.com/file/d/0B_8znW3VOlkGYnpoanBCN3BQejQ/view?usp=sharing). Get yours today!


