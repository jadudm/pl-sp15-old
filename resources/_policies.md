---
title: Course Policies
resource: local
layout: default
---

{% callout info %}
These policies may be amended, edited, or otherwise updated if deemed necessary by the instructor.
{% endcallout %}

## Office Hours

I am more than happy to chat, and sometimes you can stop by during office hours and I'll be free; however, I schedule my time pretty tightly, so it is possible I may ask you to make an appointment and/or follow up via email.

## Course Prerequisites

CSC 226 and CSC 236.

## Attendance

We will be actively engaging during class time in pair programming and group discussion activities throughout the term. Attendance is part of being professional as a student; each missed class may cost you up to 2.5% off your final grade.

If you arrive particularly late, leave particularly early, or fail to participate in class, I will consider you absent.

If you miss class for reasons that are not considered legitimate, you are responsible for getting caught up. That does not mean you should write me, apologize for being absent, and then ask me what you missed. Start with a kind classmate, and see how you fare.

### Legitimate Reasons You Might Be Absent

If you legitimately cannot be present, you will find a way to communicate that to me in advance.

* If you have flu-like symptoms, email me and visit the health center. The Center for Disease Control recommends you stay home for at least 24 hours after the symptoms pass---RESTING. Without evidence from the health center of your visit, you are not excused.

* Interviews

* Family Emergencies

I will do my best to work with you to make sure that you are caught up on anything you miss if you miss class for legitimate reasons.
 
## Technology Policies

Much of the work in this course will require use of the computer, so these policies are designed to help you better understand how to be effective in a technology-rich environment.

* **Laptop and Software**: We will regularly make use of laptops during class, and you are expected to have them unless explicitly stated otherwise.

* **Communication**: The course website is your primary source for information about this course. Messages about the course will often be sent by email. These are all mechanisms you would likely use in a professional position in the real world (corporate websites, internal content management systems, and email)---you are, likewise, expected to use them in a responsible and professional manner in this course.

* **Backups**: All students are expected to back-up their work on a daily basis, which includes laboratories, assignments, and quizzes. The best way to do this is to store a copy of all work in a cloud service such as Dropbox, SkyDrive, Google Drive, or to use a DVD, flash drive, or some other media. Storing multiple copies of something on your laptop is not a backup. I will not be sympathetic to lost work in any way, shape, or form.

## Evening Lab / Support

The Computing and Digital Crafts Lab is open Sunday through Thursday from 7:00 to 9:00 PM (except on evenings of convocations). The primary teaching assistant and several other TAs will be able to answer questions about the content in the course during consultations in their  Lab hours. You are strongly encouraged to make use of the help available in the Computing and Digital Crafts Lab, as well as in the instructors' office hours. Best results are obtained trying to solve problems before asking for help, and you should be prepared to show what you have already tried. Topics in this course build throughout the course, so you should be sure to do your best to keep up with the class, so as to not fall behind. No question to which you do not know the answer is "dumb" unless it goes unanswered because it remained unasked.
