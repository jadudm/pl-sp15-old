---
title: Software
resource: local
layout: default
---

There are a lot of different tools we'll be using this term. People have complained about this in the past. I'm afraid there's nothing I can really do about that... we need tools to do what we do!

## Dr. Racket

We will be using Racket extensively throughout the term. You should obtain the most recent version of [Racket](http://racket-lang.org/) for your operating system of choice. 

## Bitbucket

I have dreams of using [Bitbucket](https://bitbucket.org/) for managing code and collaboration. You should create an account there, and we'll see how things go.

## Cloud9

We will use the [Cloud9 environment](http://c9.io/) throughout the term, as it provides a consistent Linux virtual environment that we can install software into. You can use your Bitbucket account to log into Cloud9, meaning you have one less password to manage.

## Cel.ly

Celly will be used for alerts and updates. Students have repeatedly asked for ways they can be notified about changes to the website and other course announcements via *anything but* email. Celly provides me with a way to post messages that **you can choose** to have received via SMS, email, or via an app you can install (Android or IOS).

You can create an account using Twitter/Facebook/Google credentials, but **your profile must use your berea.edu address**. This is how I am making sure only Berea students can join into our "cells." You will want to join the cell <b><a href="http://cy.tl/1CKqzez">@ProgrammingLanguages</a></b> after you create your account. 

<div class="text-center">
  <iframe width="640" height="360" 
          src="//www.youtube.com/embed/CHAU4qjD-i4?rel=0" 
          frameborder="0" 
          allowfullscreen
          ></iframe>
<br/>
</div>

## Q.berea

Q.berea is a free/open question and answer package living at [q.berea.mobi](http://q.berea.mobi/). Please create an account here. I installed and am hosting this service.

**NOTE**: Do not use a completely garbage password, but please do not reuse your Berea password, either. I say this because I cannot provide you with a secure connection (HTTPS) for Q.berea, and therefore your password is being transmitted in the clear. For that reason, don't reuse something you care about.

