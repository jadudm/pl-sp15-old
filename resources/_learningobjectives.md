---
title: Learning Objectives
resource: local
layout: default
---

Learning objectives are the assessable points of learning in the course. The number of these learning objectives you complete is what determines your final grade in this course.

These objectives are subject to change.

## Outline

1. [(Mostly) Functional Programming in Racket](#mostly-functional-programming-in-racket)

1. [Representation and Interpretation of Languages](#representation-and-interpretation-of-languages)

1. [Learning New Languages](#learning-new-languages)

## (Mostly) Functional Programming in Racket

It is expected that it will take approximately 7 weeks to cover this material, assuming that 60% of your time is being spent on learning related to functional programming in Racket.

Objectives marked with a **?** are particularly subject to change/omission based on the lived experience of the course.

### Level 1

1. Write state-free programs over atomic data.

1. Develop tests that cover representative inputs and branches in code.

1. Demonstrate knowledge of tools for writing code (IDE, documentation)

1. Survey breadth of the standard library for software development in Racket.

### Level 2

1. Write state-free programs over structured data.

1. Discuss scope as it pertains to values bound within and passed between functions.

1. Compare/contrast functional and object-oriented programming as it pertains to notions of scope (eg. Racket/Python/C++).

1. Write useful functions using *map*, *fold*, and *filter*.

### Level 3

1. Write state-free programs over built-in lists.

1. Write state-free programs over self-defined, self-referential data.

1. Reason about scope in the presence of closures and compare/contrast with anonymous classes. **?**

1. Discuss the role of functional languages in the context of big data, concurrency, and other styles of programming. (For example, big data, safety, web-based systems, and other current challenges in computing.) **?**

1. Write useful functions that return functions.

## Representation and Interpretation of Languages

It is assumed that the implementation language for these objectives is Racket, and that it will take approximately 7 weeks to complete these objectives at 60% effort.

Objectives marked with a **?** are particularly subject to change/omission based on the lived experience of the course.


### Level 1

1. Describe an AST for a small language (eg. a BNF or EBNF).

1. Define appropriate structures for parsed output representation.

1. Parse programs in a simple, 1-type language.

1. Compare/contrast with interpreter with bytecode interpretation. **?**

1. Implement basic tests for a small interpreter.

### Level 2

1. Extend a 1-type language with conditional structures and a second type.

1. Discuss/describe types as sets.

1. Implement ribcage environments for implementing scope. 

1. Compare and contrast different approaches to scoping.

1. Implement tests for new language features.

### Level 3

1. Add rudimentary type checking to the interpreter.

1. Extend the interpreter to include 1st-class functions.

1. Compare/contrast static vs. dynamic type systems in languages.

1. Discuss the nature/undecidability of type systems.

## Learning New Languages

It is assumed that this learning objective will be repeated three, possibly four times over the course of the term. That is, each language will receive 3 weeks of learning time at 40% effort. There is only one level of competency associated with this learning task.

1. Demonstrate facility with the syntax of the language.

1. Participate in the online community of learners by asking and answering questions. 

1. Write at least one program of substance that exemplifies the design intent behind the language.

1. Know the basic history of the language as well as the current state of its community.

1. Compare/contrast this language with Racket in terms of design intent, binding and iteration forms, functions, structures, and memory management. (This involves *moving past the hype*.)

## Habits of Practice

These are not learning objectives *per se*, but instead represent practices that you are expected to engage in all term.

1. Engage in question asking and answering in Piazza, our course discussion tool.

1. Get together with your learning team(s) at least once per week outside of class, possibly twice weekly.

1. Journal regarding your learning successes and challenges. (This might also be thought of as a *learning trajectory log*, where you reflect on what objectives you are making progress on, and so on.)

These will likely be incorporated explicitly in assignments.
